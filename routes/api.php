<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('text', 'TextController@index');
Route::get('text/{id}', 'TextController@show');
Route::post('text', 'TextController@store');
Route::put('text/{id}', 'TextController@update');
Route::delete('text/{id}', 'TextController@delete');
