<?php

namespace App\Http\Controllers;

use App\Text;
use Illuminate\Http\Request;

class TextController extends Controller
{
    public function index()
    {
        return Text::all();
    }

    public function show($id)
    {
        return Text::find($id);;
    }

    public function store(Request $request)
    {
        return Text::create($request->all());
    }

    public function update(Request $request, $id)
    {
        $text = Text::findOrFail($id);
        $text->update($request->all());

        return $text;
    }

    public function delete( $id)
    {
        $text = Text::findOrFail($id);
        $text->delete();

        return 204;
    }
}
